var PlanetModel = Backbone.Model.extend({
  defaults: {
    init: null,
    planet_id: null,
    segments: 100,
    radius: null,
    vector: null,
    material: null,
    mesh: null
  },
  initialize: function(options) {
    for(var property in this.defaults) {
      this[property] = this.defaults[property];
    }
 
    for(var property in options) {
      this[property] = options[property];
    }
    this.vector = mesh = new THREE.Mesh(new THREE.Geometry());
//    this.vector = new THREE.Object3D();
    this.radius = parseInt(this.display_diameter);
    this.object = new THREE.SphereGeometry(this.radius, this.segments, this.segments); 
    this.material = new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture(this.map) });
    this.mesh = new THREE.Mesh(this.object, this.material, this.texture); 
    this.vector.add(this.mesh);
    this.mesh.rotation.x = this.axis * Math.PI / 180;
    
    this.mesh.position.x = this.display_distance;
  },
  animate: function(seconds_per_year, rotation_ratio, fps) {
    
    if (this.rotation_speed > 0) {
      var rot_degree_rotation = ((180 / (seconds_per_year * fps) * (365 / this.rotation_speed))) / rotation_ratio;
      this.mesh.rotation.y += (Math.PI / 180) * rot_degree_rotation;
    }
    
    if (this.orbital_period > 0) {
      var orbit_degree_rotation = (360 / (seconds_per_year * fps)) * (365 / this.orbital_period); 
      this.vector.rotation.y += (Math.PI / 180) * orbit_degree_rotation;
    }

if (this.planet_id == 'earth') {
//  console.log(orbit_degree_rotation);
}

    if (this.init == null) {
//console.log(this.vector);
      this.init = true;
    }

  }
  
});

