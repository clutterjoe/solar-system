var CameraModel = Backbone.Model.extend({
  events: {
    'change': 'setLookAt'
  },
  defaults: {
    lookAtObject: null,
    lookAtObjectPrevious: null,
    distance: 100000,
    field_of_view: 75,
    aspect: window.innerWidth/window.innerHeight,
    near: 0.1,
    far: 500000000,
    perspective: null,
    onMouseDownPosition: {
      x: 0,
      y: 0
    },
    theta: 45, 
    onMouseDownTheta: 45, 
    phi: 60, 
    onMouseDownPhi: 60
        
  },
  initialize: function(options) {
    for(var property in this.defaults) {
      this[property] = this.defaults[property];
    }
    for(var property in options) {
      this[property] = options[property];
    }
    this.perspective = new THREE.PerspectiveCamera(this.field_of_view, this.aspect, this.near, this.far);
    this.perspective.position.set(0, this.distance, this.distance);
    this.setAspectRatio();
    this.initial_position = this.perspective.position;

    var $this = this;
    $(window).resize(function() { 
      $this.setAspectRatio(); 
    });    
  },
  setAspectRatio: function() {
		this.perspective.aspect = window.innerWidth / window.innerHeight;
		this.renderer.setSize( window.innerWidth, window.innerHeight );
  },
  setLookAt: function(object) {
    this.lookAtObjectPrevious = this.lookAtObject;
    this.lookAtObject = object;
  },
  updateLookAt: function() {
    if (this.lookAtObject != null) {
      var vector = new THREE.Vector3();
      vector.getPositionFromMatrix( this.lookAtObject.mesh.matrixWorld );
      this.perspective.lookAt(vector);

    }
  }
});
