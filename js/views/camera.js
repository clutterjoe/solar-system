var CameraView = Backbone.View.extend({
  model: CameraModel,
  initialize: function() {
    this.perspective = this.model.perspective;


  },
  animate: function(event) {
    this.model.updateLookAt();
  },
  updateLookAt: function(lookAtObject) {
    this.model.setLookAt(lookAtObject);
//    console.log(lookAtObject);
  }
  
});
