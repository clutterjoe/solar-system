/**
 * Creates a cube that acts as the background for the scene.
 */
var sky_factory = function(dimension) {
  if (dimension === undefined) {
    dimension = 1000000;
  }
  var imagePrefix = "images/nebula-";
  var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
  var imageSuffix = ".png";
  var skyGeometry = new THREE.CubeGeometry( dimension, dimension, dimension );	

  var imageURLs = [];
  for (var i = 0; i < 6; i++) {
  	imageURLs.push( imagePrefix + directions[i] + imageSuffix );
  }
  var textureCube = THREE.ImageUtils.loadTextureCube( imageURLs );
  var shader = THREE.ShaderLib[ "cube" ];
  shader.uniforms[ "tCube" ].value = textureCube;
  var skyMaterial = new THREE.ShaderMaterial( {
  	fragmentShader: shader.fragmentShader,
  	vertexShader: shader.vertexShader,
  	uniforms: shader.uniforms,
  	depthWrite: false,
  	side: THREE.BackSide
  });
  return new THREE.Mesh( skyGeometry, skyMaterial );
}

/**
 * Loads relevant JSON file and creates a PlanetModel from the data culled from within the file.
 */
var PlanetFactory = function(planet_id, planet_parent_obj) {
  var planet = null;
  json_file = 'json/' + planet_id + '.json';
  $.ajax(json_file, {
    async: false,
    dataType: 'json',
    success: function(content) {
      planet = new PlanetModel(content);    
      if (planet_parent_obj != null) {
        planet.parent_planet = planet_parent_obj;
      }
      planet.planet_id = planet_id;
    }
  });
  return planet;
}



$(document).ready(function() {

  var rotation_ratio = 50; 
  var seconds_per_year = 30; 

  var camera_distance_from_target = 450000,  default_camera_distance_from_target = camera_distance_from_target;
  var renderer = new THREE.WebGLRenderer(); 

  var scene_model = new SceneModel();  
  var camera_model = new CameraModel({ 'renderer' : renderer, 'distance' : camera_distance_from_target }); 

  camera_model.set('distance'. camera_distance_from_target);
  camera_model.set('default_distance'. default_camera_distance_from_target);

  var planets = new PlanetCollection();  

  camera = new CameraView({model: camera_model});
  camera.perspective.lookAt(scene_model.scene.position);	

  space_bg = sky_factory(10000000);

  scene_model.scene.add(space_bg);

  renderer.setSize(window.innerWidth, window.innerHeight); 
  document.body.appendChild(renderer.domElement); 


  planets.add(PlanetFactory('sun'));
  planets.add(PlanetFactory('mercury'));
  planets.add(PlanetFactory('venus'));
  planets.add(PlanetFactory('earth'));
  planets.add(PlanetFactory('mars'));
  planets.add(PlanetFactory('moon'));
  
  planets.setParent('moon', 'earth');
  
  planets.addPlanetsToScene(scene_model.scene);

  console.log(planets.getPlanet('moon').mesh.position);

  camera.model.setLookAt(planets.getPlanet('sun'));

  $('.rotation_ratio').click(function() {
    a = $(this).attr('href').split('/');
    rotation_ratio = a[a.length-1];
  });    

  $('.annual_interval').click(function() {
    a = $(this).attr('href').split('/');
    seconds_per_year = a[a.length-1];
  });    

  $('.planet_focus').click(function() {
    a = $(this).attr('href').split('/');
    lookAtObject = planets.getPlanet(a[1]);
    controls.minDistance = planets.getPlanet(a[1]).display_diameter;
console.log( controls.minDistance );
    camera.updateLookAt(lookAtObject);
  });    


  
	var stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.bottom = '0px';
	stats.domElement.style.zIndex = 100;
	container = $('body')[0];
	container.appendChild( stats.domElement );

	var controls = new THREE.TrackballControls( camera.perspective );
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;

	controls.noZoom = false;
	controls.noPan = false;

	controls.staticMoving = true;
	controls.dynamicDampingFactor = 0.3;

  controls.minDistance = 200000;
  controls.maxDistance = 1000000;

  controls.keys = [ 65, 83, 68 ];

  function controlRender() {
    renderer.render( scene_model.scene, camera.perspective );
    stats.update();
  }    

  controls.addEventListener( 'change', controlRender );

console.log(controls);


  var lastCalledTime = new Date().getTime();
  var render = function () { 

  
    requestAnimationFrame(render); 

    controls.update();

   

    delta = (new Date().getTime() - lastCalledTime)/1000;
    if (delta == 0) {
      delta = 0.010;
    }
    lastCalledTime = new Date().getTime();
    fps = 1/delta;
    camera.animate();
    planets.animate(seconds_per_year, rotation_ratio, fps);
 
    renderer.render(scene_model.scene, camera.perspective); 
  }; 


  render();

  
});